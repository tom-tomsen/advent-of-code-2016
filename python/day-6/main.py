#!/usr/bin/env python3

import operator
from sys import stdin

def part1(data):
    column = {}

    for line in data:
        for idx, chr in enumerate(line.strip()):
            if idx not in column:
                column[idx] = {}

            if chr not in column[idx]:
                column[idx][chr] = 1
            else:
                column[idx][chr] += 1

    print("Part 1: ", end='')
    for i in range(len(column)):
        print(max(column[i], key=column[i].get), end='')
    print()
    return

def part2(data):
    column = {}

    for line in data:
        for idx, chr in enumerate(line.strip()):
            if idx not in column:
                column[idx] = {}

            if chr not in column[idx]:
                column[idx][chr] = 1
            else:
                column[idx][chr] += 1

    print("Part 2: ", end='')
    for i in range(len(column)):
        print(min(column[i], key=column[i].get), end='')
    return

def main():
    data = stdin.readlines()
    part1(data)
    part2(data)

if __name__ == "__main__":
    print(f"\nDay 6")
    main()
