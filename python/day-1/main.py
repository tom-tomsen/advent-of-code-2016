#!/usr/bin/env python3

from sys import stdin

dir = [
    (-1, 0),
    (0, 1),
    (1, 0),
    (0, -1),
]

turn = {
    'R': 1,
    'L': -1,
}

def part1(data):
    x = 0
    y = 0
    look = 0

    instr = [(c[0], int(c[1:])) for c in data]

    for (d, l) in instr:
        look = (look + turn[d])  % 4

        x += dir[look][0] * l
        y += dir[look][1] * l

    print(f"Part 1: {abs(x) + abs(y)}")


def part2(data):
    x = 0
    y = 0
    look = 0
    dic = dict()
    firstIntersection = None

    instr = [(c[0], int(c[1:])) for c in data]

    for (d, len) in instr:
        look = (look + turn[d])  % 4

        for _ in range(len):
            x += dir[look][0]
            y += dir[look][1]

            if (x, y) in dic:
                firstIntersection = (x, y)
                break
            else:
                dic[(x, y)] = (x, y)

        if firstIntersection:
            break

    print(f"Part 2: {abs(firstIntersection[0]) + abs(firstIntersection[1])}")

def main():
    data = stdin.readlines()[0].strip().split(", ")
    part1(data)
    part2(data)

if __name__ == "__main__":
    print(f"\nDay 1")
    main()
