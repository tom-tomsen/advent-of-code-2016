#!/usr/bin/env python3

# better solution from burnybug@reddit: https://www.reddit.com/r/adventofcode/comments/5gy1f2/2016_day_7_solutions/davzcwy/
# original solution (see git history)

from sys import stdin
import re

def abba(x):
    return any(a == d and b == c and a != b for a, b, c, d in zip(x, x[1:], x[2:], x[3:]))

def part1(data):
    lines = [re.split(r'\[([^\]]+)\]', line.strip()) for line in data]
    parts = [(' '.join(p[::2]), ' '.join(p[1::2])) for p in lines]
    print('Answer #1:', sum(abba(sn) and not(abba(hn)) for sn, hn in parts))

def part2(data):
    lines = [re.split(r'\[([^\]]+)\]', line.strip()) for line in data]
    parts = [(' '.join(p[::2]), ' '.join(p[1::2])) for p in lines]
    print('Answer #2:', sum(any(a == c and a != b and b+a+b in hn for a, b, c in zip(sn, sn[1:], sn[2:])) for sn, hn in parts))

def main():
    data = stdin.readlines()
    part1(data)
    part2(data)

if __name__ == "__main__":
    print(f"\nDay 7")
    main()
