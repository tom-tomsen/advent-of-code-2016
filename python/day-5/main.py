#!/usr/bin/env python3

import hashlib
from sys import stdin

def part1(salt):
    password = []

    i = 0
    while len(password) < 8:
        digest = hashlib.md5((salt + str(i)).encode('utf-8')).hexdigest()
        if digest.startswith("00000"):
            password.append(digest[5])

        i += 1

    print(f"Part 1: {''.join(password)}")
    return

def part2(salt):
    password = {}

    i = 0
    while len(password) < 8:
        digest = hashlib.md5((salt + str(i)).encode('utf-8')).hexdigest()
        if digest.startswith("00000"):
            pos = int(digest[5], 16)

            if pos in range(8) and pos not in password:
                password[pos] = str(digest[6])

        i += 1

    print(f"Part 2: {''.join([str(password[k]) for k in range(8)])}")
    return

def main():
    data = stdin.readlines()[0].strip()
    part1(data)
    part2(data)

if __name__ == "__main__":
    print(f"\nDay 5")
    main()
