#!/usr/bin/env python3

from sys import stdin

def part1(data):
    return

def part2(data):
    return

def main():
    data = stdin.readlines()[0].strip().split(", ")
    part1(data)
    part2(data)

if __name__ == "__main__":
    print(f"\nDay 1")
    main()
