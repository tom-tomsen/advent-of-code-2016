#!/usr/bin/env python3

from sys import stdin

def part1(data):
    inv = 0

    for line in data:
        a,b,c = map(int, line.strip().split())
        if not (a+b <= c or b+c <= a or c+a <= b):
            inv += 1

    print(f"Part 1: {inv}")
    return

def part2(data):
    inv = 0

    for i in range(0, len(data), 3):
        a1, b1, c1 = map(int, data[i].strip().split())
        a2, b2, c2 = map(int, data[i+1].strip().split())
        a3, b3, c3 = map(int, data[i+2].strip().split())

        if not (a1+a2 <= a3 or a2+a3 <= a1 or a3+a1 <= a2):
            inv += 1

        if not (b1+b2 <= b3 or b2+b3 <= b1 or b3+b1 <= b2):
            inv += 1

        if not (c1+c2 <= c3 or c2+c3 <= c1 or c3+c1 <= c2):
            inv += 1

    print(f"Part 2: {inv}")
    return

def main():
    data = stdin.readlines()
    part1(data)
    part2(data)

if __name__ == "__main__":
    print(f"\nDay 1")
    main()
