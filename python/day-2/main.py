#!/usr/bin/env python3

from sys import stdin

dir = {
    'U': (0, -1),
    'D': (0, 1),
    'R': (1, 0),
    'L': (-1, 0),
}

def part1(data):

    pad = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]

    pos = [1, 1]
    code = list()

    for line in data:
        for c in line:
            dx = dir[c][0]
            if dx < 0 and pos[0] > 0 or dx > 0 and pos[0] < 2:
                pos[0] += dx

            dy = dir[c][1]
            if dy < 0 and pos[1] > 0 or dy > 0 and pos[1] < 2:
                pos[1] += dy

        code.append(str(pad[pos[1]][pos[0]]))

    print(f"Part 1: {''.join(code)}")

def part22(data):

    pad = {
        (0, 2): '1',
        (1, 1): '2',
        (1, 2): '3',
        (1, 3): '4',
        (2, 0): '5',
        (2, 1): '6',
        (2, 2): '7',
        (2, 3): '8',
        (2, 4): '9',
        (3, 1): 'A',
        (3, 2): 'B',
        (3, 3): 'C',
        (4, 2): 'D'
    }

    pos = (2, 2)
    code = list()

    for line in data:
        for c in line:
            dx = dir[c][0]
            if (pos[0], pos[1] + dx) in pad:
                pos = (pos[0], pos[1] + dx)

            dy = dir[c][1]
            if (pos[0] + dy, pos[1]) in pad:
                pos = (pos[0] + dy, pos[1])

        code.append(pad[pos])

    print(f"Part 2: {''.join(code)}")

def main():
    data = [(c.strip()) for c in stdin.readlines()]
    part1(data)
    part22(data)

if __name__ == "__main__":
    print(f"\nDay 2")
    main()
